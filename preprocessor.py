from sys import argv;

def enqueue(queue, item):
    if type(queue) == list:
        return queue + [item];
    return queue + item;

def dequeue(queue, length=1):
    if length > len(queue):
        return queue;
    return queue[length:];

def peek(queue, length=1):
    if length > len(queue):
        return None;
    if length == 1:
        return queue[0];
    return queue[:length];

def roll(queue, length=1):
    if length > len(queue):
        return queue;
    return queue[length:] + queue[:length];

def seek(queue, item):
    while peek(queue) != item:
        queue = roll(queue);
    return queue;

def tokenize(string):
    return ['_' + token for token in string.split()];

def preprocess(tokens=[], definitions={}):
    steps = 0;
    tokens = ["BEGIN"] + enqueue(tokens, "END");
    while steps != len(tokens):
        if peek(tokens) == "_:":
            tokens = dequeue(tokens);
            name = peek(tokens);
            tokens = dequeue(tokens);
            definition = [];
            depth = 1;
            while peek(tokens) != "END":
                if peek(tokens) == "_:":
                    depth += 1
                elif peek(tokens) == "_;":
                    depth -= 1
                if depth == 0:
                    break;
                definition = enqueue(definition, peek(tokens));
                tokens = dequeue(tokens);
            tokens = dequeue(tokens);
            definitions[name] = definition;
            steps = 0;
        elif peek(tokens) in definitions:
            for token in definitions[peek(tokens)]:
                tokens = enqueue(tokens, token);
            tokens = seek(dequeue(tokens), "BEGIN");
            steps = 0;
        else:
            tokens = roll(tokens);
            steps += 1;
    return (tokens, definitions);

def accumulate(tokens):
    return ",".join([token[1:] for token in tokens if token not in {"BEGIN", "END"}]);

def help(name):
    print(name + ":", "A simple preprocessor.");
    print("Usage:", name, "<file> <file> <file> ...");

def main():
    if len(argv) < 2:
        help(argv[0]);
        return;
    definitions = {};
    for number in range(0, pow(2, 16)):
        definitions['_' + str(number)] = [
                                            "_new_number",
                                            "_last"
                                         ] + [
                                            "_digit_" + digit
                                            for digit in bin(number)[2:]
                                         ] + [
                                            "_duplicate", "_last", "_drop",
                                         ];
        definitions["_$" + str(number)] = definitions['_' + str(number)] + ["_last"];
    for file in argv[1:]:
        with open(file) as file:
            content = file.read();
        output, definitions = preprocess(tokenize(content), definitions);
        print(accumulate(output));

if __name__ == "__main__":
    main();
