#include <stdio.h>

#define queue_size    1024
#define process_count 256
#define program_size  (sizeof(program) / sizeof(instruction))

#define wrap_forward(cursor)   \
    if(cursor >= queue_size) { \
        cursor = 0;            \
    }

#define wrap_backward(cursor)    \
    if(cursor >= queue_size) {   \
        cursor = queue_size - 1; \
    }

#define next(cursor)     cursor++; wrap_forward(cursor)
#define previous(cursor) cursor--; wrap_backward(cursor)

#define enqueue(item)                                       \
    if(processes[self].count < queue_size) {                \
        processes[self].queue[processes[self].head] = item; \
        next(processes[self].head);                         \
        processes[self].count++;                            \
    }

#define dequeue                                    \
    = processes[self].queue[processes[self].tail]; \
    next(processes[self].tail);                    \
    processes[self].count--;

#define guard(arity)                    \
    if(processes[self].count < arity) { \
        return 0;                       \
    }

#define unary_operation(name, operation) \
    cell name(cell self) {               \
        cell first;                      \
        guard(1)                         \
        first dequeue                    \
        enqueue(operation(first))        \
        processes[self].cursor++;        \
        return 1;                        \
    }

#define binary_operation(name, operation) \
    cell name(cell self) {                \
        cell first;                       \
        cell second;                      \
        guard(2)                          \
        first dequeue                     \
        second dequeue                    \
        enqueue(first operation second)   \
        processes[self].cursor++;         \
        return 1;                         \
    }

#define _(operation) cell operation(cell);

typedef unsigned long cell;
typedef struct process process;
typedef cell (*instruction)(cell);
struct process {
    cell queue[queue_size];
    cell head;
    cell tail;
    cell count;
    cell cursor;
};

_(duplicate)
_(drop)
_(swap)
_(last)
_(new_number)
_(digit_0)
_(digit_1)
_(not)
_(bitwise_not)
_(add)
_(subtract)
_(multiply)
_(divide)
_(modulo)
_(and)
_(or)
_(bitwise_and)
_(bitwise_or)
_(equal)
_(less)
_(greater)
_(shift_left)
_(shift_right)
_(loop_begin)
_(loop_end)
_(conditional_break)
_(halt)
_(spawn)
_(terminate)
_(handle)
_(send)
_(receive)
_(get_character)
_(put_character)
_(print_queue)

process     processes[process_count];
instruction program[] = {
    #include "program.h"
};

cell duplicate(cell self) {
    cell item;
    guard(1) {
        item dequeue
        enqueue(item)
        enqueue(item)
    }
    processes[self].cursor++;
    return 1;
}

cell drop(cell self) {
    guard(1)
    next(processes[self].tail)
    processes[self].count--;
    processes[self].cursor++;
    return 1;
}

cell swap(cell self) {
    cell first;
    cell second;
    guard(2)
    first dequeue
    second dequeue
    enqueue(second)
    enqueue(first)
    processes[self].cursor++;
    return 1;
}

cell last(cell self) {
    guard(1)
    previous(processes[self].head)
    previous(processes[self].tail)
    processes[self].queue[processes[self].tail] = processes[self].queue[processes[self].head];
    processes[self].cursor++;
    return 1;
}

cell new_number(cell self) {
    enqueue(0)
    processes[self].cursor++;
    return 1;
}

cell digit_0(cell self) {
    guard(1)
    processes[self].queue[processes[self].tail] <<= 1;
    processes[self].cursor++;
    return 1;
}

cell digit_1(cell self) {
    guard(1)
    processes[self].queue[processes[self].tail] = (processes[self].queue[processes[self].tail] << 1) | 1;
    processes[self].cursor++;
    return 1;
}

unary_operation(not,          !)
unary_operation(bitwise_not,  ~)

binary_operation(add,         +)
binary_operation(subtract,    -)
binary_operation(multiply,    *)
binary_operation(divide,      /)
binary_operation(modulo,      %)
binary_operation(and,         &&)
binary_operation(or,          ||)
binary_operation(bitwise_and, &)
binary_operation(bitwise_or,  |)
binary_operation(equal,       ==)
binary_operation(less,        <)
binary_operation(greater,     >)
binary_operation(shift_left,  <<)
binary_operation(shift_right, >>)

cell loop_begin(cell self) {
    cell condition;
    cell depth;
    guard(1)
    depth = 0;
    condition dequeue
    while(!condition) {
        if(program[processes[self].cursor] == loop_begin) {
            depth++;
        }
        else if(program[processes[self].cursor] == loop_end) {
            depth--;
        }
        if(depth == 0) {
            break;
        }
        processes[self].cursor++;
    }
    processes[self].cursor++;
    return 1;
}

cell loop_end(cell self) {
    cell depth = 0;
    while(processes[self].cursor < program_size) {
        if(program[processes[self].cursor] == loop_end) {
            depth++;
        }
        else if(program[processes[self].cursor] == loop_begin) {
            depth--;
        }
        if(depth == 0) {
            break;
        }
        processes[self].cursor--;
    }
    return 1;
}

cell conditional_break(cell self) {
    cell condition;
    cell depth;
    guard(1)
    depth = 1;
    condition dequeue
    while(!condition) {
        if(program[processes[self].cursor] == loop_begin) {
            depth++;
        }
        else if(program[processes[self].cursor] == loop_end) {
            depth--;
        }
        if(depth == 0) {
            break;
        }
        processes[self].cursor++;
    }
    processes[self].cursor++;
    return 1;
}

cell halt(cell self) {
    return 0;
}

cell spawn(cell self) {
    cell iterator;
    cell depth;
    for(iterator = 0;
        iterator < process_count;
        iterator++) {
        if(processes[iterator].cursor >= program_size) {
            processes[iterator].cursor = processes[self].cursor + 1;
            enqueue(iterator)
            break;
        }
    }
    if(iterator >= process_count) {
        return 0;
    }
    depth = 0;
    while(processes[self].cursor < program_size) {
        if(program[processes[self].cursor] == spawn) {
            depth++;
        }
        else if(program[processes[self].cursor] == terminate) {
            depth--;
        }
        if(depth == 0) {
            break;
        }
        processes[self].cursor++;
    }
    processes[self].cursor++;
    return 1;
}

cell terminate(cell self) {
    processes[self].cursor = program_size;
    return 1;
}

cell handle(cell self) {
    enqueue(self);
    processes[self].cursor++;
    return 1;
}

cell send(cell self) {
    cell first;
    cell second;
    cell temporary;
    guard(2)
    first = processes[self].queue[processes[self].tail];
    if(first >= process_count) {
        return 0;
    }
    if(program[processes[first].cursor] == receive) {
        first dequeue
        second dequeue
        temporary = self;
        self = first;
        enqueue(second)
        processes[first].cursor++;
        processes[temporary].cursor++;
    }
    return 1;
}

cell receive(cell self) {
    return 1;
}

cell get_character(cell self) {
    enqueue((cell)getchar())
    processes[self].cursor++;
    return 1;
}

cell put_character(cell self) {
    cell item;
    guard(1)
    item dequeue
    putchar((char)item);
    processes[self].cursor++;
    return 1;
}

void init() {
    cell iterator;
    for(iterator = 1;
        iterator < process_count;
        iterator++) {
        processes[iterator].cursor = program_size;
    }
    return;
}

cell print_queue(cell self) {
    cell iterator;
    iterator = processes[self].tail;
    printf("%lu : ", self);
    while(iterator != processes[self].head) {
        printf("%lu ", processes[self].queue[iterator]);
        iterator++;
        wrap_forward(iterator)
    }
    printf("\n");
    processes[self].cursor++;
    return 1;
}

void run() {
    cell iterator;
    cell stop;
    stop = 0;
    while(!stop) {
        stop = 1;
        for(iterator = 0;
            iterator < process_count;
            iterator++) {
            if(processes[iterator].cursor          >= program_size ||
               program[processes[iterator].cursor] == receive) {
                continue;
            }
            program[processes[iterator].cursor](iterator);
            stop = 0;
        }
    }
    return;
}

int main(void) {
    init();
    run();
    return 0;
}
